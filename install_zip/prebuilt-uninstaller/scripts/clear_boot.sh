#!/sbin/sh
BUSYBOX="/tmp/busybox"
LZ4="/tmp/lz4"
BOOT_DEV="$(cat /tmp/bootdev)"
RD_ADDR="$(cat /tmp/rd_addr)"

CMPR_GZIP=0
CMPR_LZ4=1

if [ ! -e "$BOOT_DEV" ]; then
    echo "BOOT_DEV \"$BOOT_DEV\" does not exist!"
    return 1
fi

dd if=$BOOT_DEV of=/tmp/boot.img
/tmp/bbootimg -x /tmp/boot.img /tmp/bootimg.cfg /tmp/zImage /tmp/initrd.img /tmp/second.img /tmp/dtb.img
if [ ! -f /tmp/zImage ] ; then
    echo "Failed to extract boot.img"
    return 1
fi

use_mediatek_extra_headers=false
magic=$($BUSYBOX hexdump -n 4 -v -e '/1 "%02X"' "/tmp/initrd.img")
if [ "$magic" == "88168858" ]; then
    use_mediatek_extra_headers=true
fi

if [ "$use_mediatek_extra_headers" == "true" ]; then
    dd if=/tmp/initrd.img of=/tmp/initrd.img-mediatek-header bs=512 count=1
    dd if=/tmp/initrd.img of=/tmp/initrd.img-headerless bs=512 skip=1
    mv /tmp/initrd.img-headerless /tmp/initrd.img
fi

rm -r /tmp/boot
mkdir /tmp/boot

cd /tmp/boot
rd_cmpr=-1
magic=$($BUSYBOX hexdump -n 4 -v -e '/1 "%02X"' "../initrd.img")
case "$magic" in
    1F8B*)           # GZIP
        $BUSYBOX gzip -d -c "../initrd.img" | $BUSYBOX cpio -i
        rd_cmpr=CMPR_GZIP;
        ;;
    02214C18)        # LZ4
        $LZ4 -d "../initrd.img" stdout | $BUSYBOX cpio -i
        rd_cmpr=CMPR_LZ4;
        ;;
    *)
        echo "invalid ramdisk magic $magic"
        ;;
esac

if [ rd_cmpr == -1 ] || [ ! -f /tmp/boot/init ] ; then
    echo "Failed to extract ramdisk!"
    return 1
fi

# restore init
if [ -e /tmp/boot/main_init ] ; then
    rm /tmp/boot/init
    mv /tmp/boot/main_init /tmp/boot/init
fi

chmod 750 /tmp/boot/init

# restore ueventd and watchdogd symlink
if [ -L /tmp/boot/sbin/ueventd ] ; then
    ln -sf ../init /tmp/boot/sbin/ueventd
fi
if [ -L /tmp/boot/sbin/watchdogd ] ; then
    ln -sf ../init /tmp/boot/sbin/watchdogd
fi

if [ -e /tmp/boot/mrom.fstab ] ; then
    rm /tmp/boot/mrom.fstab
fi

# Remove encryption files
rm -rf /tmp/boot/mrom_enc

# pack the image again
cd /tmp/boot

case $rd_cmpr in
    CMPR_GZIP)
        find . | $BUSYBOX cpio -o -H newc | $BUSYBOX gzip > "../initrd.img"
        ;;
    CMPR_LZ4)
        find . | $BUSYBOX cpio -o -H newc | $LZ4 stdin "../initrd.img"
        ;;
esac

echo "bootsize = 0x0" >> /tmp/bootimg.cfg
if [ -n "$RD_ADDR" ]; then
    echo "Using ramdisk addr $RD_ADDR"
    echo "ramdiskaddr = $RD_ADDR" >> /tmp/bootimg.cfg
fi

cd /tmp

dtb_cmd=""
if [ -f "dtb.img" ]; then
    dtb_cmd="-d dtb.img"
fi

if [ "$use_mediatek_extra_headers" == "true" ]; then
    mv /tmp/initrd.img /tmp/initrd.img-headerless

    # The header must include the size of the ramdisk in a little-endian field
    # of four bytes. The value provided by wc is the ASCII representation of the
    # size as a big-endian decimal number, so a conversion is needed.
    #
    # The ASCII representation of the big-endian decimal number is first
    # converted to the ASCII representation of a big-endian hexadecimal number
    # of eight digits and padded with zeroes. This is split in four chunks of
    # two hexadecimal digits each and then reversed, thus getting a list with
    # the four ASCII representations of each of the little-endian bytes of the
    # number. Finally, that list is converted from the ASCII representation to
    # its literal value and written to the header as a little-endian number of
    # four bytes.
    size=$($BUSYBOX wc -c /tmp/initrd.img-headerless | $BUSYBOX cut -f 1 -d ' ')
    hex_size=$($BUSYBOX printf %08X $size)
    reversed_tokenized_hex_size=$(echo -n "$hex_size" | while read -n2 asciiByte; do echo "$asciiByte"; done | $BUSYBOX tac)

    # Note that the resulting number must be passed directly to dd, as null
    # characters (those resulting from converting "00" to its literal value) can
    # not be stored in variables in Bash, but they can be transfered through
    # pipes.
    for asciiByte in $reversed_tokenized_hex_size; do $BUSYBOX printf "\x$asciiByte"; done | dd conv=notrunc of=/tmp/initrd.img-mediatek-header bs=1 seek=4 count=4

    mv /tmp/initrd.img-mediatek-header /tmp/initrd.img
    cat /tmp/initrd.img-headerless >> /tmp/initrd.img
    rm /tmp/initrd.img-headerless
fi

/tmp/bbootimg --create newboot.img -f bootimg.cfg -k zImage -r initrd.img $dtb_cmd

if [ ! -e "/tmp/newboot.img" ] ; then
    echo "Failed to inject boot.img!"
    return 1
fi

echo "Writing new boot.img..."
dd bs=4096 if=/tmp/newboot.img of=$BOOT_DEV
return $?
